Nix-Flakes + Docker = <3
========================

What is this?
-------------
This is a Docker container that has the latest pre-release version of Nix with flake support installed. It is useful in CI environments, such as GitLab CI.

What are flakes?
----------------
Flakes are a [proposed standard](https://github.com/nixos/rfcs/pull/49) for reproducible Nix expressions. A flake is a git repository that contains a flake.nix that specifies the inputs of this expression (allowing to pin and override them for stability or integration) and outputs, which are usually derivations.

How does this work?
-------------------
This container is built with Nixpkgs' dockerTools. It contains the latest version of Nix, coreutils, git and some minor things, such as cacert. Optionally you could roll your own (WIP) with the Nix function mkNixDockerImage.

Is this any good?
-----------------
Mmm... probably! This container can build itself, by the way, as a proof-of-concept.

The `.gitlab-ci.yml`'s `build` stage is a great template for a quick start.

Did you do this yourself?
-------------------------
Nope! This is based on [LnL7's work](https://github.com/LnL7/nix-docker) which I adapted to Nix-Flakes and cleaned up a bit.
