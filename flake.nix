{
  description = "Nix for Docker - now with 100% more flakes! Part of a complete breakfast.";
  edition = 201909;

  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "nixos-unstable";
    };
  };
  outputs = { self, nixpkgs }: {
    lib = {
      mkNixDockerImage = {
        # The instantiated nixpkgs. Use like this: import nixpkgs { system = <system>; };
        nixpkgs,
        # Extra substituters. { url = "...", publicKey = "..."; }
        extraSubstituters ? [],
        # Extra packages, either from the nixpkgs set that was passed
        # or a different one (not recommended, the image size might grow)
        extraPackages ? [],
        # Command that will launch the container, it's passed to the entrypoint script
        cmd ? null,
        # Extra commands run in dockerTools.buildImage installPhase.
        # Use them to register new users and manipulate the image contents at build time.
        extraBuildCommands ? "",
        # Commands run on container launch. Please try to keep them idempotent.
        extraRuntimeCommands ? ""
      }: let
        substituters = [{
          url = "https://cache.nixos.org/";
          # Don't forget to be paranoid and verify the key.
          publicKey = "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=";
        }] ++ extraSubstituters;
        pkgs = nixpkgs;
        inherit (pkgs) lib;
        path = pkgs.buildEnv {
          name = "system-path";
          paths = with pkgs; [
            bashInteractive coreutils cacert git
            nixFlakes
            (shadow.override { pam = null; })
          ] ++ extraPackages;
        };
        entrypoint = ''
          #!${pkgs.bashInteractive}/bin/bash
          nix-store --init && nix-store --load-db < .reginfo
          mkdir -m 1777 -p /tmp
          mkdir -p /nix/var/nix/gcroots /nix/var/nix/profiles/per-user/root /var/empty /root
          ln -sf ${path} /nix/var/nix/gcroots/booted-system
          ln -sf /nix/var/nix/profiles/per-user/root/profile /root/.nix-profile
          ${extraRuntimeCommands}
          exec "$@"
        '';
        nixconf = ''
          build-users-group = nixbld
          sandbox = false
          experimental-features = ca-references nix-command flakes fetch-tree
          substituters = ${lib.concatStringsSep " " (map (s: s.url) substituters)}
          trusted-public-keys =  ${lib.concatStringsSep " " (map (s: s.publicKey) substituters)}
          require-sigs = true
        '';

        passwd = ''
          root:x:0:0::/root:/run/current-system/sw/bin/bash
          ${lib.concatStringsSep "\n" (lib.genList (i: "nixbld${toString (i+1)}:x:${toString (i+30001)}:30000::/var/empty:/run/current-system/sw/bin/nologin") 32)}
        '';

        group = ''
          root:x:0:
          nogroup:x:65534:
          nixbld:x:30000:${lib.concatStringsSep "," (lib.genList (i: "nixbld${toString (i+1)}") 32)}
        '';
        nsswitch = ''
          hosts: files dns myhostname mymachines
        '';
      in pkgs.dockerTools.buildImage {
        name = "registry.gitlab.com/vikanezrimaya/nix-flakes-docker";
        tag = "${self.rev}";
        created = "now";
        contents = pkgs.stdenvNoCC.mkDerivation {
          name = "user-environment";
          phases = [ "installPhase" "fixupPhase" ];
          exportReferencesGraph = map (drv: [("closure-" + baseNameOf drv) drv]) [ path ];
          installPhase = ''
            mkdir -p $out/run/current-system $out/var
            ln -s /run $out/var/run
            ln -s ${path} $out/run/current-system/sw

            mkdir -p $out/bin $out/usr/bin $out/sbin
            ln -s ${pkgs.stdenv.shell} $out/bin/sh
            ln -s ${pkgs.coreutils}/bin/env $out/usr/bin/env

            mkdir -p $out/etc/nix
            echo '${nixconf}' > $out/etc/nix/nix.conf
            echo '${passwd}' > $out/etc/passwd
            echo '${group}' > $out/etc/group
            echo '${nsswitch}' > $out/etc/nsswitch.conf

            echo '${entrypoint}' > $out/entrypoint
            chmod 555 $out/entrypoint

            ${extraBuildCommands}

            printRegistration=1 ${pkgs.perl}/bin/perl ${pkgs.pathsFromGraph} closure-* > $out/.reginfo
          '';
        };
        config.EntryPoint = [ "/entrypoint" ];
        config.Cmd = (if cmd != null then cmd else ["${pkgs.bashInteractive}/bin/bash"]);
        config.Env = [
          "PATH=/root/.nix-profile/bin:/run/current-system/sw/bin"
          "MANPATH=/root/.nix-profile/share/man:/run/current-system/sw/share/man"
          "NIX_PAGER=cat"
          "NIX_SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
        ];
      };
    };
    packages = let
      genPackageSet = system: let
        pkgs = import nixpkgs { inherit system; }; 
      in {
        remote-builder = self.lib.mkNixDockerImage {
          nixpkgs = pkgs;
          extraPackages = with pkgs; [ openssh ];
          extraBuildCommands = ''
            echo "sshd:x:498:65534::/var/empty:/run/current-system/sw/bin/nologin" >> $out/etc/passwd
            mkdir -p $out/etc/ssh
            cp ${pkgs.openssh}/etc/ssh/sshd_config $out/etc/ssh
            sed -i '/^PermitRootLogin/d' $out/etc/ssh/sshd_config
            echo "PermitRootLogin yes" >> $out/etc/ssh/sshd_config

            cat >> $out/etc/bashrc <<EOF
            export PATH=/root/.nix-profile/bin:/run/current-system/sw/bin
            export NIX_SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
            export MANPATH=/root/.nix-profile/share/man:/run/current-system/sw/share/man
            EOF

            echo source /etc/bashrc >> $out/etc/profile
          '';
          extraRuntimeCommands = ''
            ssh-keygen -A
            mkdir /root/.ssh -p
            if [[ $SSH_PUBLIC_KEY ]]; then
              echo $SSH_PUBLIC_KEY > /root/.ssh/authorized_hosts
            else
              echo No SSH public key was added. Access will be impossible. Exiting. >&2
              echo 'If you want to access this remote builder, add an SSH public key in SSH_PUBLIC_KEY environment variable.'
              exit 1
            fi
          '';
        };
        ci-builder = self.lib.mkNixDockerImage { nixpkgs = pkgs; };
      };
    in {
      x86_64-linux = genPackageSet "x86_64-linux";
      aarch64-linux = genPackageSet "aarch64-linux";
    };
    defaultPackage = {
      x86_64-linux =  self.packages.x86_64-linux.ci-builder;
      aarch64-linux = self.packages.aarch64-linux.ci-builder;
    };
  };
}
